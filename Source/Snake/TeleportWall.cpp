// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportWall.h"
#include "Ekans.h"
#include "SnakeElementBase.h"

// Sets default values
ATeleportWall::ATeleportWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeleportWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<AEkans>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SnakeElements[0]->TeleportTo(Teleport,FRotator(0,0,0),false,false);
		}
	}
}

