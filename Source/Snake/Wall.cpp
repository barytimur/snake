#include "Wall.h"
#include "Ekans.h"
#include "Kismet/GameplayStatics.h"

AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<AEkans>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
		Restart();
	}
}
void AWall::Restart()
{
	UGameplayStatics::OpenLevel(this, FName(UGameplayStatics::GetCurrentLevelName(this)));
}