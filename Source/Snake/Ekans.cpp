// Fill out your copyright notice in the Description page of Project Settings.


#include "Ekans.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstance.h"
#include "Food.h"


// Sets default values
AEkans::AEkans()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 10.f;
}

// Called when the game starts or when spawned
void AEkans::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	if (UGameplayStatics::GetCurrentLevelName(this) == "Level_1")
	{
		NextLevelName = "Level_2";
	}
	else if (UGameplayStatics::GetCurrentLevelName(this) == "Level_2")
	{
		NextLevelName = "Level_3";
	}
}

// Called every frame
void AEkans::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	if (PointCount >= NextLevelPoint)
	{
		UGameplayStatics::OpenLevel(this, FName(NextLevelName));
	}

}

void AEkans::AddSnakeElement(int ElementsNum)
{	
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(-(SnakeElements.Num() *ElementSize), 0, 0);
		FRotator NewRotation(0.f, 0.f, 0.f);
		FVector NewScale(0.75f, 0.75f, 0.75f);
		FTransform NewTransform(NewRotation, NewLocation, NewScale);
		ASnakeElementBase* SnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementalClass, NewTransform);
		SnakeElem->SnakeOwner = this;
		int32 IndexElem = SnakeElements.Add(SnakeElem);
		if (IndexElem == 0)
		{
			SnakeElem->SetFirstElementType();
		}
	}
}
void AEkans::Move()
{
	FVector MovementVector = { 0,0,0 };
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		rot = { 90,0,0 };
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		rot = { 90,0,0 };
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		rot = { 90,90,0 };
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		rot = { 90,90,0 };
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->MeshComponent->SetVisibility(true);
	SnakeElements[0]->MeshComponent->SetWorldRotation(rot);

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		SnakeElements[i]->MeshComponent->SetVisibility(true);
		SnakeElements[i]->MeshComponent->SetWorldRotation(rot);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void AEkans::SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor* other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
}
