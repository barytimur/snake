#include "PlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h" 
#include "Ekans.h"
#include "Food.h"
#include "Components/InputComponent.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"



APlayerPawn::APlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	PawnCamera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	PawnCamera->SetOrthoWidth(1200);
	PawnCamera->SetAspectRatio(1);
	PawnCamera->SetHiddenInGame(true);
	RootComponent = PawnCamera;
}

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	CreateFoodActor();
	
	/*LevelName = UGameplayStatics::GetCurrentLevelName(this);
	switch (AllLevels)
	{
	case Level_Names::Level_1:
		NextLevelName = FName("Level_2");
		AllLevels = Level_Names::Level_2;
		break;
	case Level_Names::Level_2:
		NextLevelName = FName("Level_3");
		AllLevels = Level_Names::Level_3;
		break;
	case Level_Names::Level_3:
		NextLevelName = FName("Level_4");
		AllLevels = Level_Names::Level_4;
		break;
	case Level_Names::Level_4:
		break;
	}*/
	/*APlayerController* PC = Cast<APlayerController>(GetController());
	UWidgetBlueprintLibrary::SetInputMode_GameOnly(PC);*/
}

void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsValid(ASweety) == false)
	{
		CreateFoodActor();
	}
	
}

void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawn::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawn::HandlePlayerHorizontalInput);


}

void APlayerPawn::CreateSnakeActor()
{
	AEkansActor = GetWorld()->SpawnActor<AEkans>(AEkansActorT, FTransform());
}

void APlayerPawn::CreateFoodActor()
{
	float _X = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	float _Y = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	FVector NewLocation(_X, _Y, 0);
	FTransform NewTransform(NewLocation);
	FActorSpawnParameters param;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	ASweety = GetWorld()->SpawnActor<AFood>(AFoodActorT, NewTransform, param);
}

void APlayerPawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(AEkansActor))
	{
		if (value > 0 && (AEkansActor->LastMoveDirection != EMovementDirection::DOWN))
		{
			AEkansActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && (AEkansActor->LastMoveDirection != EMovementDirection::UP))
		{
			AEkansActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawn::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(AEkansActor))
	{
		if (value > 0 && (AEkansActor->LastMoveDirection != EMovementDirection::LEFT))
		{
			AEkansActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && (AEkansActor->LastMoveDirection != EMovementDirection::RIGHT))
		{
			AEkansActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

