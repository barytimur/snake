// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Engine/EngineTypes.h"
#include "TimerManager.h"
#include "Bonus.generated.h"




UCLASS()
class SNAKE_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
	void StopBonus();
};
