#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class ABonus;

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AFood();
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> Foods;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonus> Bonuses;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	//template<class T>
	UFUNCTION()
	void SpawnNewFood();

	UFUNCTION()
	void SpawnBonuses();
};
