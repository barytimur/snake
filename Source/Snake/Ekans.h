#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ekans.generated.h"

class ASnakeElementBase;
class AFood;
UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UENUM()
enum class Level_Names
{
	Level_1,
	Level_2,
	Level_3,
};

UCLASS()
class SNAKE_API AEkans : public AActor
{
	GENERATED_BODY()
	
public:	
	AEkans();
	FRotator rot;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementalClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	int NextLevelPoint = 20;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(BlueprintReadOnly)
	int PointCount = 0;

	FString NextLevelName;
	

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};
