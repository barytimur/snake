// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "Ekans.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TimerDel.BindUFunction(this, FName("StopBonus"));
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();		
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<AEkans>(Interactor);
		if (IsValid(Snake))
		{
			this->Destroy();
			Snake->SetActorTickInterval(0.1);
			Snake->PointCount += 1;
			GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 5.f, false);
			//UKismetSystemLibrary::K2_SetTimer(this, "StopBonus", 5, false, 0.0F, 0.0F);
		}
	}
}

void ABonus::StopBonus()
{
	AEkans Snake;
	Snake.SetActorTickInterval(0.2);
}