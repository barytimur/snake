#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

class UCameraComponent;
class AEkans;
class AFood;



UCLASS()
class SNAKE_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	APlayerPawn();
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	AFood* ASweety;

	UPROPERTY(BlueprintReadWrite)
	AEkans* AEkansActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AEkans>	 AEkansActorT;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood>	 AFoodActorT;


protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void CreateSnakeActor();
	void CreateFoodActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
	

};
