#include "Food.h"
#include "Ekans.h"
#include "Bonus.h"
#include "Kismet/KismetMathLibrary.h"


AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<AEkans>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->Destroy();
			Snake->PointCount+=1;
		}
		if(UKismetMathLibrary::RandomIntegerInRange(0, 10) == 0)
		{
			SpawnBonuses();
		}
	}
}
//template <class T>
void AFood::SpawnNewFood()
{
	float _X = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	float _Y = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	FVector NewLocation(_X, _Y, 0);
	FTransform NewTransform(NewLocation);
	FActorSpawnParameters param;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
	GetWorld()->SpawnActor<AFood>(Foods, NewTransform,param );
}
void AFood::SpawnBonuses()
{
	float _X = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	float _Y = UKismetMathLibrary::RandomFloatInRange(-400, 400);
	FVector NewLocation(_X, _Y, 0);
	FTransform NewTransform(NewLocation);
	FActorSpawnParameters param;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	GetWorld()->SpawnActor<ABonus>(Bonuses, NewTransform, param);
}